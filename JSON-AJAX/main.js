// -----------------JSON------------------
// object and propeties

// var myCat = {
//     "name": "Mewsalot",
//     "species": "cat",
//     "favFood" : "tuna",
// }
// if i need to access to propety of my cat
// myCat.favFood or myCat.name

// arrays

// var myFavColor = ["blue","black", "purple"]

// if I need access to value or position in my array 
// myFavColor[1] or myFavColor[0]


// ----------------------AJAX-----------

// apernet----enermaint villa urquiza ---it patagonia---retail
// ahoralo que hichimos es escribiros datosenconsola, pero ahora le vamos a asignar un evento con el boton paraque imprima den pantalla.
// var ourRequest = new XMLHttpRequest();
// // RECIBIMOS DATA
// ourRequest.open('GET', 'https://learnwebcode.github.io/json-example/animals-1.json');
// ourRequest.onload = function() {
//     // console.log(ourRequest.responseText);
//     var ourData = JSON.parse(ourRequest.responseText);
//     console.log(ourData[0]);

// };
// // enviamos el reuqest
// ourRequest.send();

// ----------------------creamos el evento -----------------------------------------
var pageCounter = 1;
var animalContainer = document.getElementById("animal-info");
var btn = document.getElementById("btn");

btn.addEventListener("click", function () {
    var ourRequest = new XMLHttpRequest();
    // RECIBIMOS DATA
    ourRequest.open('GET', 'https://learnwebcode.github.io/json-example/animals-' + pageCounter + '.json');
    ourRequest.onload = function () {
        // console.log(ourRequest.responseText);
        var ourData = JSON.parse(ourRequest.responseText);
        renderHTML(ourData);
    };
    // enviamos el reuqest
    ourRequest.send();
    pageCounter++;

    if (pageCounter > 3) {
        btn.classList.add("hide ME");
    }
});

// CON ESTA FUNCION LLAMAMOS AL JSON AL HTML
function renderHTML(data) {
    var htmlString = "";

    for (i = 0; i < data.length; i++) {
        htmlString += "<p>" + data[i].name + "is a " + data[i].species + "that likes to eat" data[i]."</p>";
    }
    animalContainer.insertAdjacentHTML('beforeend', htmlString)
};