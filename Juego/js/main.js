const WIDTH = 400;
const HEIGH = 400;

let target = {
    x: getRandomNumber(WIDTH),
    y: getRandomNumber(HEIGH)
};

let $map = document.getElementById('map');
let $distance = document.getElementById('distance');
let clicks = 0;

$map.addEventListener('click', function (e) {
    clicks++;
    let distance = getdistance(e, target);
    let distancehint = getDistanceHint(distance);
    // console.log(distancehint)
    $distance.innerHTML = `<h1>${distancehint}</h1>`;

    if (distance < 20) {
        alert('Found the treasure in ${clicks} clicks!');
        location.reload();
    }
});