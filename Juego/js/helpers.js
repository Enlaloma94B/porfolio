// creamos todas las funciones que vamos a reutilizar
// genera numeros aleatorios
// 
let getRandomNumber = size => {
    return Math.floor(Math.random() * size);
    
}

let getdistance = (e, target) => {
    let diffX = e.offsetX - target.x;
    let diffY = e.offsetY - target.y;
    return Math.sqrt((diffX * diffX) + (diffY * diffY));
}

let getDistanceHint = distance => {
    if (distance < 30) {
        return "boiling hot";
    } else if (distance < 40) {
        return "Really hot";
    } else if (distance < 60) {
        return "hot";
    } else if (distance < 100) {
        return "warm";
    } else if (distance < 180) {
        return "cold"; 
    } else if (distance < 360) {
        return "really cold";
    } else {
        return "freezing";
    }


}